<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>TASK MANAGER</title>
</head>
<style>
    h1 {
        font-size: 1.6 emp;
    }

    a {
        color: darkblue;
    }

    select {
        width: 200px;
    }

    input[type="text"] {
        width: 200px;
    }

    input[type="date"] {
        width: 200px;
    }

</style>
<body>
<table width="100%" height="100%" border="1" style="padding: 10px;">
    <tr>
        <sec:authorize access="isAuthenticated()">
            <td height="35" width="200" nowrap="nowrap" align="center">
                <a href="/"><b>TASK MANAGER</b></a>
            </td>
            <td width="100%" align="right">
                <a href="/projects">PROJECTS </a>
                |
                <a href="/tasks"> TASKS </a>
                |
                User: <sec:authentication property="name"/>
                |
                <a href="/logout"> LOGOUT </a>
            </td>
        </sec:authorize>

        <sec:authorize access="!isAuthenticated()">
            <td width="100%" align="right">
                <a href="/login">LOGIN</a>
            </td>
        </sec:authorize>

    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="padding: 10px;">
