package ru.t1.godyna.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.event.ConsoleEvent;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    private final String NAME = "exit";

    @NotNull
    private final String DESCRIPTION = "Close application";

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.exit(0);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
