package ru.t1.godyna.tm.exceprion.field;

public final class LoginEmptyException extends AbsractFieldException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}
