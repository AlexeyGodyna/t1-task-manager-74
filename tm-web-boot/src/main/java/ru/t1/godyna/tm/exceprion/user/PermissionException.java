package ru.t1.godyna.tm.exceprion.user;


public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
