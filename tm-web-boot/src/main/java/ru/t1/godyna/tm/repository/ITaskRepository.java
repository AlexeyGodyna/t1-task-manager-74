package ru.t1.godyna.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.godyna.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends JpaRepository<Task, String> {

    void deleteAllByUserId(String userId);

    @Nullable
    List<Task> findAllByUserId(String userId);

    void deleteByIdAndUserId(String id, String userId);

    @Nullable
    Task findByIdAndUserId(String id, String userId);

}
