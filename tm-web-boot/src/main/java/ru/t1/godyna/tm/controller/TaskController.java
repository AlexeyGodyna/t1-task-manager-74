package ru.t1.godyna.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.model.CustomUser;
import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.service.ProjectService;
import ru.t1.godyna.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;

    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        taskService.addByUserId(new Task("New Task " + System.currentTimeMillis()), user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeByIdAndUserId(id, user.getUserId());
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        taskService.addByUserId(task, user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        @Nullable final Task task = taskService.findOneByIdAndUserId(id, user.getUserId());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(user));
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Status[] getStatuses() {
        return Status.values();
    }

    private Collection<Project> getProjects(@AuthenticationPrincipal final CustomUser user) {
        return projectService.findAllByUserId(user.getUserId());
    }

}
