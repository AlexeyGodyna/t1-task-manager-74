package ru.t1.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.godyna.tm.api.endpoint.ITaskEndpoint;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.service.TaskService;
import ru.t1.godyna.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.godyna.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    public  Collection<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findOneByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.addByUserId(task, UserUtil.getUserId());
        return task;
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.removeByUserId(task, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(){
        taskService.removeAllByUserId(UserUtil.getUserId());
    }

}
