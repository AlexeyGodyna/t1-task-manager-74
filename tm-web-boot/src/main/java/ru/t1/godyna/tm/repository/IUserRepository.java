package ru.t1.godyna.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.godyna.tm.model.User;

public interface IUserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull String login);

    boolean existsByLogin(@NotNull String login);

}
