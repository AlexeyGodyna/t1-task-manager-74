package ru.t1.godyna.tm.exceprion.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.exceprion.AbstractException;

@NoArgsConstructor
public abstract class AbsractFieldException extends AbstractException {

    public AbsractFieldException(@NotNull final String message) {
        super(message);
    }

    public AbsractFieldException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbsractFieldException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbsractFieldException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
