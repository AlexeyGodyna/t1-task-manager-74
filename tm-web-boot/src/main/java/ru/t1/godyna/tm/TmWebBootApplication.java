package ru.t1.godyna.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmWebBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmWebBootApplication.class, args);
	}

}
