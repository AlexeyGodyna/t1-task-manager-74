package ru.t1.godyna.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.godyna.tm.model.CustomUser;
import ru.t1.godyna.tm.service.ProjectService;
import ru.t1.godyna.tm.service.TaskService;

@Controller
public class TasksController {

    @Autowired
    TaskService taskService;

    @Autowired
    ProjectService projectService;

    @GetMapping("/tasks")
    public ModelAndView index(
            @AuthenticationPrincipal final CustomUser user
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAllByUserId(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}
