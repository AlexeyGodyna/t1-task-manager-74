package ru.t1.godyna.tm.exceprion.field;

public final class EmailEmptyException extends AbsractFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
