package ru.t1.godyna.tm.exceprion.field;

public final class TaskIdEmptyException extends AbsractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}
