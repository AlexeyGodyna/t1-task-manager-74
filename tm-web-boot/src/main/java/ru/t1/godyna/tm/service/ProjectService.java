package ru.t1.godyna.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.exceprion.user.AccessDeniedException;
import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.repository.IProjectRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Transactional
    public void addByUserId(@Nullable final Project model, @Nullable final String userId) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        model.setUserId(userId);
        projectRepository.save(model);
    }

    @Transactional
    public void update(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.findAllByUserId(userId);
    }

    @Transactional
    public void remove(@Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeByUserId(@Nullable final Project model, @Nullable final String userId) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Transactional
    public void removeAllByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Nullable
    public Project findOneById(@Nullable String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    public Project findOneByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

}
